A bare bones GraphQL Api to expose the countries and continents
data from package countries-list

How to run:
docker-compose up
Should be accessible on localhost:4000

Alternatively npm install && node server.js should also work