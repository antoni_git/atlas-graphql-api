const { ApolloServer, print } = require('apollo-server');
const { resolvers } = require('./resolvers');
const { typeDefs } = require('./schema'); 
const { logPlugin } = require('./logPlugin');  

const server = new ApolloServer({ typeDefs, resolvers, plugins: [logPlugin]}); 
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
