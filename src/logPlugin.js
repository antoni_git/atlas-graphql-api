exports.logPlugin = {
  requestDidStart(_requestContext) { 
    return { 
      willSendResponse(requestContext) {
        if(requestContext.request.operationName != 'IntrospectionQuery'){   
          console.log(`Timestamp: ${Date.now()}`);
          console.log('Result:');
          console.log(JSON.stringify(requestContext.response.data));
          console.log('Query:');
          console.log(requestContext.request.query);
        }          
      }
    }
  }
} 