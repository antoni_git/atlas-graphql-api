const { countries, continents } = require('countries-list');

const usableCountries = [];
Object.keys(countries).forEach((key) => {
  usableCountries.push(Object.assign(countries[key], { code: key }));
}); 

exports.resolvers = {
  Country: {
    continent(obj, args, _context, _info) {
      let code;
      if (!args.code) {
        code = obj.continent;
      } else {
        code = args.code;
      }
      return {
        code,
        name: continents[code],
      };
    },
  },
  Continent: {
    countries(obj, _args, _context, _info) {
      return usableCountries.filter((country) => country.continent === obj.code);
    },
  },
  Query: {
    continents: (_obj, args, _context, _info) => {
      const continentsArray = [];
      Object.keys(continents).forEach((code) => {
        const continentObj = {
          name: continents[code],
          code,
          countries: [],
        };
        usableCountries.forEach((country) => {
          if (country.continent === code) {
            continentObj.countries.push(country);
          }
        });
        continentsArray.push(continentObj);
      });
      return continentsArray;
    },
    continent: (_obj, args, _context, _info) => {
      return {
        name: continents[args.code],
        code: args.code,
      };
    },
    countries: (_obj, args, _context, _info) => {
      return usableCountries;
    },
    country: (_obj, args, _context, _info) => {
      return Object.assign(countries[args.code], { code: args.code });
    },
  },
};